#! /usr/bin/env perl6

use v6.c;

use String::Fold;
use Test;

plan 5;

subtest "Empty string" => {
	plan 1;

	my Str $result = fold("");

	is $result, "", "Empty string returns another empty string";
}

subtest "Single short line" => {
	plan 1;

	my $result = slurp("t/files/input/single-short-line.txt").trim.&fold;

	is $result, slurp("t/files/output/single-short-line.txt").trim, "Short line returns the same short line";
}

subtest "Single long line" => {
	my $result = slurp("t/files/input/single-long-line.txt").trim.&fold();
	my @expected-lines = slurp("t/files/output/single-long-line.txt").trim.lines;
	my @result-lines = $result.lines;

	plan 1 + @expected-lines.elems;

	is @result-lines.elems, @expected-lines.elems, "Expected {@result-lines.elems} lines";

	for ^@expected-lines {
		is @result-lines[$_], @expected-lines[$_], "Line {$_ + 1} is correct";
	}
}

subtest "Multiple short lines" => {
	my $result = slurp("t/files/input/multiple-short-lines.txt").trim.&fold();
	my @expected-lines = slurp("t/files/output/multiple-short-lines.txt").trim.lines;
	my @result-lines = $result.lines;

	plan 1 + @expected-lines.elems;

	is @result-lines.elems, @expected-lines.elems, "Expected {@result-lines.elems} lines";

	for ^@expected-lines {
		is @result-lines[$_], @expected-lines[$_], "Line {$_ + 1} is correct";
	}
}

subtest "Multiple long lines" => {
	my $result = slurp("t/files/input/multiple-long-lines.txt").trim.&fold();
	my @expected-lines = slurp("t/files/output/multiple-long-lines.txt").trim.lines;
	my @result-lines = $result.lines;

	plan 1 + @expected-lines.elems;

	is @result-lines.elems, @expected-lines.elems, "Expected {@result-lines.elems} lines";

	for ^@expected-lines {
		is @result-lines[$_], @expected-lines[$_], "Line {$_ + 1} is correct";
	}
}

# vim: ft=perl6 noet
