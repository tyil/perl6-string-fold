#! /usr/bin/env perl6

use v6.c;

use String::Fold;
use Test;

plan 1;

subtest "Three paragraphs" => {
	my @result-lines = fold(slurp("t/files/input/three-paragraphs.txt")).lines;
	my @expected-lines = slurp("t/files/output/three-paragraphs.txt").trim.lines;

	plan 1 + @expected-lines.elems;

	is @result-lines.elems, @expected-lines.elems, "Expected number of lines";

	for ^@expected-lines {
		is @result-lines[$_], @expected-lines[$_], "Line {$_ + 1} is the same";
	}
}

# vim: ft=perl6 noet
