# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic
Versioning](http://semver.org/spec/v2.0.0.html).

## [0.2.0] - 2019-08-05

### Changed

The method of folding strings has been rewritten, to hopefully work better in
all circumstances.

### Removed

The `indent` parameter has been removed from `&fold`. This functionality is
left up to the user, and can still be easily achieved by lowering the `width`
parameter and calling `&indent` from the upstream code.

## [0.1.0] - 2018-07-08

- Initial release
